// 1st problem:
function divisibleBy(x) {
	if (x % 3 == 0 && x % 5 == 0) {
		return "FizzBuzz";
	} else if (x % 5 == 0) {
		return "Buzz";
	} else if (x % 3 == 0) {
		return "Fizz";
	} else {
		return "Pop";
	}
}

// Test cases:
console.log(divisibleBy(5));
console.log(divisibleBy(30));
console.log(divisibleBy(27));
console.log(divisibleBy(17));


// 2nd problem:
function rainbow(letter) {
	let cleanedLetter = letter.toUpperCase();
	switch (cleanedLetter) {
		case "R":
			return "Red";
			break;
		case "O":
			return "Orange";
			break;
		case "Y":
			return "Yellow";
			break;
		case "G":
			return "Green";
			break;
		case "B":
			return "Blue";
			break;
		case "I":
			return "Indigo";
			break;
		case "V":
			return "Violet";
			break;
		default:
			return "No color";
	}
}

// Test cases
console.log(rainbow("r"));
console.log(rainbow("G"));
console.log(rainbow("x"));
console.log(rainbow("B"));
console.log(rainbow("i"));


// Stretch goal
// Medyo nakakasabog ng utak!
function leapOrNot(year) {
	if (year % 4 == 0 && year % 100 == 0 && year % 400 == 0) {
		return "Leap year";
	} else if (year % 4 == 0 && year % 100 !== 0) {
		return "Leap year";
	} else if (year % 4 == 0 && year % 100 == 0 && year % 400 !== 0) {
		return "Not a leap year"
	} else {
		return "Not a leap year"
	}
}

// Test cases
console.log(leapOrNot(1900));
console.log(leapOrNot(2000));
console.log(leapOrNot(2004));
console.log(leapOrNot(2021));